import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BienvComponent } from './pages/bienv/bienv.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { RegistroComponent } from './pages/registro/registro.component';
import { SearchComponent } from './pages/search/search.component';
import { OftaComponent } from './pages/ofta/ofta.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'registro', component: RegistroComponent},
  {path: 'bienvenida', component: BienvComponent},
  {path: 'home', component: HomeComponent},
  {path: 'search', component: SearchComponent},
  {path: 'ofta', component: OftaComponent},
  {path: '**', redirectTo: '/login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
