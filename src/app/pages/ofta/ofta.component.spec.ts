import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OftaComponent } from './ofta.component';

describe('OftaComponent', () => {
  let component: OftaComponent;
  let fixture: ComponentFixture<OftaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OftaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OftaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
